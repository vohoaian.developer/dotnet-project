using Microsoft.EntityFrameworkCore;

namespace SportsStore.Models
{
  public static class SeedData
  {
    public static void EnsurePopulated(IApplicationBuilder app)
    {
      StoreDbContext context = app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<StoreDbContext>();
      if (context.Database.GetPendingMigrations().Any())
      {
        context.Database.Migrate();
      }

      if (!context.Products.Any())
      {
        context.Products.AddRange(
        new Product { Name = "Product 1", Description = "Description 1", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.1", Description = "Description 1.1", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.2", Description = "Description 1.2", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.3", Description = "Description 1.3", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.4", Description = "Description 1.4", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.5", Description = "Description 1.5", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.6", Description = "Description 1.6", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.7", Description = "Description 1.7", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.8", Description = "Description 1.8", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.9", Description = "Description 1.9", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 1.10", Description = "Description 1.10", Category = "Category1", Price = 275.95m },
        new Product { Name = "Product 2", Description = "Description 2", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.1", Description = "Description 2.1", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.2", Description = "Description 2.2", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.3", Description = "Description 2.3", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.4", Description = "Description 2.4", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.5", Description = "Description 2.5", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.6", Description = "Description 2.6", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.7", Description = "Description 2.7", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.8", Description = "Description 2.8", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.9", Description = "Description 2.9", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 2.10", Description = "Description 2.10", Category = "Category2", Price = 25.95m },
        new Product { Name = "Product 3", Description = "Description 3", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.1", Description = "Description 3.1", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.2", Description = "Description 3.2", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.3", Description = "Description 3.3", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.4", Description = "Description 3.4", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.5", Description = "Description 3.5", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.6", Description = "Description 3.6", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.7", Description = "Description 3.7", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.8", Description = "Description 3.8", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.9", Description = "Description 3.9", Category = "Category3", Price = 30 },
        new Product { Name = "Product 3.10", Description = "Description 3.10", Category = "Category3", Price = 30 },
        new Product { Name = "Product 4", Description = "Description 4", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.1", Description = "Description 4.1", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.2", Description = "Description 4.2", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.3", Description = "Description 4.3", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.4", Description = "Description 4.4", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.5", Description = "Description 4.5", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.6", Description = "Description 4.6", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.7", Description = "Description 4.7", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.8", Description = "Description 4.8", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.9", Description = "Description 4.9", Category = "Category4", Price = 27 },
        new Product { Name = "Product 4.10", Description = "Description 4.10", Category = "Category4", Price = 27 },
        new Product { Name = "Product 5", Description = "Description 5", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.1", Description = "Description 5.1", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.2", Description = "Description 5.2", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.3", Description = "Description 5.3", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.4", Description = "Description 5.4", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.5", Description = "Description 5.5", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.6", Description = "Description 5.6", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.7", Description = "Description 5.7", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.8", Description = "Description 5.8", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.9", Description = "Description 5.9", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 5.10", Description = "Description 5.10", Category = "Category5", Price = 45.05m },
        new Product { Name = "Product 6", Description = "Description 6", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.1", Description = "Description 6.1", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.2", Description = "Description 6.2", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.3", Description = "Description 6.3", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.4", Description = "Description 6.4", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.5", Description = "Description 6.5", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.6", Description = "Description 6.6", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.7", Description = "Description 6.7", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.8", Description = "Description 6.8", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.9", Description = "Description 6.9", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 6.10", Description = "Description 6.10", Category = "Category6", Price = 79.15m },
        new Product { Name = "Product 7", Description = "Description 7", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.1", Description = "Description 7.1", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.2", Description = "Description 7.2", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.3", Description = "Description 7.3", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.4", Description = "Description 7.4", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.5", Description = "Description 7.5", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.6", Description = "Description 7.6", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.7", Description = "Description 7.7", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.8", Description = "Description 7.8", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.9", Description = "Description 7.9", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 7.10", Description = "Description 7.10", Category = "Category7", Price = 22.55m },
        new Product { Name = "Product 8", Description = "Description 8", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.1", Description = "Description 8.1", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.2", Description = "Description 8.2", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.3", Description = "Description 8.3", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.4", Description = "Description 8.4", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.5", Description = "Description 8.5", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.6", Description = "Description 8.6", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.7", Description = "Description 8.7", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.8", Description = "Description 8.8", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.9", Description = "Description 8.9", Category = "Category8", Price = 100 },
        new Product { Name = "Product 8.10", Description = "Description 8.10", Category = "Category8", Price = 100 },
        new Product { Name = "Product 9", Description = "Description 9", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.1", Description = "Description 9.1", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.2", Description = "Description 9.2", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.3", Description = "Description 9.3", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.4", Description = "Description 9.4", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.5", Description = "Description 9.5", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.6", Description = "Description 9.6", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.7", Description = "Description 9.7", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.8", Description = "Description 9.8", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.9", Description = "Description 9.9", Category = "Category9", Price = 363 },
        new Product { Name = "Product 9.10", Description = "Description 9.10", Category = "Category9", Price = 363 },
        new Product { Name = "Product 10", Description = "Description 10", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.1", Description = "Description 10.1", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.2", Description = "Description 10.2", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.3", Description = "Description 10.3", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.4", Description = "Description 10.4", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.5", Description = "Description 10.5", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.6", Description = "Description 10.6", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.7", Description = "Description 10.7", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.8", Description = "Description 10.8", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.9", Description = "Description 10.9", Category = "Category10", Price = 99.5m },
        new Product { Name = "Product 10.10", Description = "Description 10.10", Category = "Category10", Price = 99.5m }
        );

        context.SaveChanges();
      }
    }
  }
}