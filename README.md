# PROJECT

## Initial Setup

```
dotnet new globaljson --sdk-version 6.0.201 --output SportsSln/SportsStore
dotnet new web --no-https --output SportsSln/SportsStore --framework net6.0
dotnet new sln -o SportsSln
dotnet sln SportsSln add SportsSln/SportsStore
```

# Set up Unittest

```
dotnet new xunit -o SportsSln/SportsStore.Tests --framework net6.0
dotnet sln SportsSln add SportsSln/SportsStore.Tests
dotnet add SportsSln/SportsStore.Tests reference SportsSln/SportsStore
dotnet add SportsSln/SportsStore.Tests package Moq --version 4.16.1
```

## Set up EF

```
dotnet add package Microsoft.EntityFrameworkCore.Design --version 6.0.0
dotnet add package Microsoft.EntityFrameworkCore.Sqlite --version 6.0.0
```

```
dotnet tool install --global dotnet-ef --version 6.0.0
```

### Create the Database Migration

```
dotnet ef migrations add Initial
# Orders
dotnet ef migrations add Orders
```

## Libman

```
dotnet tool install --global Microsoft.Web.LibraryManager.Cli --version 2.1.113
libman init -p cdnjs
libman install bootstrap@5.1.3 -d wwwroot/lib/bootstrap
libman install font-awesome@5.15.4 -d wwwroot/lib/font-awesome
```
